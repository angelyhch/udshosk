/* polyspace<MISRA-C3:1.1:Not a defect:Justify with annotations> */ 
/** 
 * @file         : \\can.h
 * @author       : Liubingqian
 * @brief        : 
 * @
 * @par          : 
 * @Copyright    : Copyright (c) 2023 AOTECAR, All Rights Reserved. 
 */
/*------------------------------------------------------------------------------
            R E V I S I O N   H I S T O R Y                                     
-------------------------------------------------------------------------------
  Date           Version        Author       Description                        
-------------------------------------------------------------------------------
2020.11.17       01.00.00       linmin        Creation                          
------------------------------------------------------------------------------*/

#ifndef _CAN_H_
#define _CAN_H_
#include "common.h"
#include "kf32a_basic_can.h"
#include "KF32A_BASIC.h"
#include "header.h"


#define M_create_std_acr(code)		((uint32_t)(code)<<21u)
#define M_create_std_msk(code)		(((uint32_t)(code)<<21u)|(0x001FFFFBu))
#define CANID_EAC_DIAG_PHY_REQ		(0x773)
#define CANID_EAC_DIAG_RES			(0x77B)
#define CANID_DIAG_FUN_REQ			(0x7DF)
#define HARDWARE_ARBITRATION   		1///<hardware arbitration

typedef enum
{
    CAN_BAUDRATE_125K = 0,
    CAN_BAUDRATE_250K = 1,
    CAN_BAUDRATE_500K = 2,
    CAN_BAUDRATE_1M   = 3
} CAN_BaudRateT;
typedef enum
{
    CAN_ERROR_NOERROR = 0,///<no error
    CAN_ERROR_BUFFERFULL=1,
} CAN_ErrorT;

extern void can_init(void); ///<can initlize
extern CAN_ErrorT CAN_Transmit_DATA(CAN_SFRmap* CANx,
						uint32_t  TargetID,
						uint8_t*  thing ,
						uint8_t   lenth,
						uint32_t  MsgType,
						uint32_t  RmtFrm
						);///<hardware send data