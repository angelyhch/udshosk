# CAN总线刷写上位机

## 介绍

  上位机集成CAN总线UDS诊断

## 说明

### FlashDownloader.getSegmentsData()

使用cfg文件定义的crc算法对传入的数据进行分段,并计算每段数据的crc值,返回一个字典,字典的key为每段数据的crc值,字典的value为每段数据的字节流

### FlashDownloader.download()

程序下载线程,使用Thread模块进行线程操作

### FlashDownloader.stop()

停止程序下载线程

### FlashDownloader.prePrograming()

程序预编译流程,包括改变会话,检查预编译条件,关闭dtc,所有指令均通过client发送,返回为response值,如响应超时会报p2超时错误

### FlashDownloader.serverPrograming()

程序编程流程,解锁安全等级,读取字典FBLDataBatch的key值,计算memory_location后发送请求下载指令,返回肯定响应后计算最大block数,随后按照block最大值发送数据,结束后发送退出下载指令,通过例程服务计算crc值,app刷写流程同上

### FlashDownloader.postPrograming()

编译后阶段,打开dtc,会话控制,返回下载成功标志
