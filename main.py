# -*- coding: gb2312 -*-
import binascii
import logging
import sys
import tkinter
import tkinter as tk
from tkinter import ttk, Radiobutton, IntVar
from tkinter import filedialog
import time
from tkinter.ttk import Checkbutton
import logging.config

import autosar
import serial
from can.interfaces.ics_neovi import NeoViBus
from can.interfaces.vector import VectorBus
from can import logger as can_logger
from cantools.database.can.c_source import generate, Message
from udsoncan.connections import PythonIsoTpConnection
from udsoncan.client import Client
from udsoncan.exceptions import *
import udsoncan.Response
import tkinter.filedialog
from loguru import logger
from sdk import cfg
from sdk.Download import FlashDownloader
from sdk.Diagnostic import Diagnostics
from sdk.Conver import Conversion
from sdk.ldf_c_source import ldf_generate
from sdk.toomosslib import ToomossBus
from sdk.Test import Test
from sdk.c_source import generate as my_generate
from sdk.cfg import *
import cantools
import os
import ldfparser

PATH = os.getcwd()
GRPBOX_WIDTH = 200
DIAG_HEIGHT = 270
DIAG_WIDTH = 500
WIDGHT_WIDTH = GRPBOX_WIDTH + DIAG_WIDTH + 30
WIDGHT_HEIGHT = DIAG_HEIGHT + 100
MAX_RCV_NUM = 20
UdsLogging = False


class RedictStdout:
    def __init__(self, scroll_text):
        # 保存原始sys.stdout进行备份
        self.save_stdout = sys.stdout
        # sys.stdout重定向，指向该对象
        sys.stdout = self
        self.scroll_text = scroll_text

    def write(self, message):
        # message即sys.stdout接收到的输出信息
        self.scroll_text.insert(tkinter.END, message)  # 在多行文本控件最后一行插入message
        # self.scroll_text.update()  # 更新显示的文本
        self.scroll_text.see(tkinter.END)  # 始终显示最后一行，不加这句，当文本溢出控件最后一行时，可能不会自动显示最后一行

    def restore(self):
        # 恢复标准输出流
        sys.stdout = self.save_stdout


class Widgets(tk.Tk, FlashDownloader, Diagnostics, Test, Conversion, RedictStdout):
    def __init__(self):
        tk.Tk.__init__(self)
        tk.Tk.resizable(self, height=False, width=False)
        tk.Tk.title(self, 'UDS HOST V1.1')
        self.time = None
        self.isopen = False
        self.messagename = None
        self.AnalysisDbcFilePath = None
        self.client = None
        self.stack = None
        self.bus = None
        self.WidgetsInit()
        RedictStdout.__init__(self, scroll_text=self.log)
        # if UdsLogging:
        # logging.config.fileConfig(os.getcwd()+"sdk\\logging.conf")
        if os.path.isfile('udsoncan.log'):
            if os.path.getsize('udsoncan.log') >= 8192000:
                os.remove('udsoncan.log')
        udsoncan.setup_logging()
        # self.logger = logging.getLogger('Mylogger')
        # else:
        self.logger = logger
        logger.add(sys.stdout, backtrace=False, format="{time:YYYY-MM-DD at HH:mm:ss} | {level} | {message}")
        logger.add('logger/file.log', retention="10 days", backtrace=True)
        logger.debug('UI生成成功')
        FlashDownloader.__init__(self, client=self.client, stack=self.stack, MyLogger=self.logger,
                                 progressbar=self.progressbarOne, time=self.time)
        Diagnostics.__init__(self, client=self.client, stack=self.stack, MyLogger=self.logger)
        Test.__init__(self, client=self.client, stack=self.stack, MyLogger=self.logger)
        Conversion.__init__(self, MyLogger=self.logger)

    def gettime(self):
        self.labelProgressbar.configure(text=str(int(self.progressbarOne['value'])) + "%")  # 重新设置标签文本
        self.time = time.time()
        self._dev_frame.after(200, self.gettime)  # 每隔0.2s调用函数 gettime 自身获取时间

    def WidgetsInit(self):
        self._dev_frame = tk.Frame(self)
        self._dev_frame.grid(row=0, column=0, padx=2, pady=2, sticky=tk.NW)

        # Device connect group
        self.gbDevConnect = tk.LabelFrame(self._dev_frame, height=100, width=GRPBOX_WIDTH, text="设备选择")
        self.gbDevConnect.grid_propagate(True)
        self.gbDevConnect.grid(row=0, column=0, padx=2, pady=2, sticky=tk.N)
        self.DevConnectWidgetsInit()

        self.gbCANCfg = tk.LabelFrame(self._dev_frame, height=190, width=GRPBOX_WIDTH, text="通道配置")
        self.gbCANCfg.grid(row=1, column=0, padx=2, pady=2, sticky=tk.NSEW)
        self.gbCANCfg.grid_propagate(True)
        self.CANChnWidgetsInit()

        self.gbDevInfo = tk.LabelFrame(self._dev_frame, height=230, width=GRPBOX_WIDTH, text="连接信息")
        self.gbDevInfo.grid(row=2, column=0, padx=2, pady=2, sticky=tk.NSEW)
        self.gbDevInfo.grid_propagate(True)
        self.DevInfoWidgetsInit()

        self.gbDiag = ttk.Notebook(height=DIAG_HEIGHT, width=DIAG_WIDTH + 6)
        self.gbDiag.grid(row=0, column=1, padx=2, pady=2, sticky=tk.NSEW)
        self.gbDiag.grid_propagate(True)
        self.frameesc = tk.Frame(self.gbDiag)
        self.gbDiag.add(self.frameesc, text="   诊断   ")
        self.frameeps = tk.Frame(self.gbDiag)
        self.gbDiag.add(self.frameeps, text="   刷写   ")
        self.frametest = tk.Frame(self.gbDiag)
        self.gbDiag.add(self.frametest, text="   测试   ")
        self.framefile = tk.Frame(self.gbDiag)
        self.gbDiag.add(self.framefile, text="   文件转换   ")
        self.analysis = tk.Frame(self.gbDiag)
        self.gbDiag.add(self.analysis, text="   DBC解析   ")
        # self.gbDiag.bind('<<NotebookTabChanged>>', self.onTabChange)

        self.log = tk.Text(width=100, height=20)
        self.log.grid(row=1, column=0, rowspan=2, columnspan=2)
        scroll = tkinter.Scrollbar()
        scroll.grid(row=1, column=2, rowspan=1, columnspan=1, ipady=100)
        scroll.config(command=self.log.yview)
        self.log.config(yscrollcommand=scroll.set)

        self.DiagWidgetsInit()
        self.FlashWidgetsInit()
        self.TestWidgetsInit()
        self.FileWidgetsInit()
        self.AnalysisWidgetsInit()

    def clearBox(self):
        self.log.delete("1.0", "end")

    def DevConnectWidgetsInit(self):
        # Device Type
        tk.Label(self.gbDevConnect, text="设备类型:").grid(row=0, column=0, sticky=tk.E)
        self.cmbDevType = ttk.Combobox(self.gbDevConnect, width=16, state="readonly")
        self.cmbDevType.grid(row=0, column=1, sticky=tk.N)
        self.cmbDevType["value"] = ('canoe', 'toomoss', 'valuecan', 'pcan', 'kvaser', 'ixxat')
        self.cmbDevType.current(0)

        # Device Index
        tk.Label(self.gbDevConnect, text="设备索引:").grid(row=1, column=0, sticky=tk.E)
        self.cmbDevIdx = ttk.Combobox(self.gbDevConnect, width=16, state="readonly")
        self.cmbDevIdx.grid(row=1, column=1, sticky=tk.N)
        self.cmbDevIdx["value"] = tuple([i for i in range(4)])
        self.cmbDevIdx.current(0)

    def CANChnWidgetsInit(self):
        # CAN Channel
        tk.Label(self.gbCANCfg, anchor=tk.W, text="CAN通道:").grid(row=0, column=0, sticky=tk.W)
        self.cmbCANChn = ttk.Combobox(self.gbCANCfg, width=12, state="readonly")
        self.cmbCANChn.grid(row=0, column=1, sticky=tk.E)
        self.cmbCANChn["value"] = tuple([i for i in range(2)])
        self.cmbCANChn.current(0)

        # Work Mode
        # tk.Label(self.gbCANCfg, anchor=tk.W, text="工作模式:").grid(row=1, column=0, sticky=tk.W)
        # self.cmbCANMode = ttk.Combobox(self.gbCANCfg, width=12, state="readonly")
        # self.cmbCANMode.grid(row=1, column=1, sticky=tk.E)
        # self.cmbCANMode["value"] = ('正常模式', '自发自收', '监控模式')
        # self.cmbCANMode.current(0)

        # CAN Baudrate
        tk.Label(self.gbCANCfg, anchor=tk.W, text="波特率:").grid(row=2, column=0, sticky=tk.W)
        self.cmbBaudrate = ttk.Combobox(self.gbCANCfg, width=12, state="readonly")
        self.cmbBaudrate.grid(row=2, column=1, sticky=tk.E)
        self.cmbBaudrate["value"] = ('500Kbps', '1000Kbps')
        self.cmbBaudrate.current(0)

        # CAN FD
        # tk.Label(self.gbCANCfg, anchor=tk.W, text="CAN FD:").grid(row=3, column=0, sticky=tk.W)
        # self.cmbFd = ttk.Combobox(self.gbCANCfg, width=12, state="readonly")
        # self.cmbFd.grid(row=3, column=1, sticky=tk.E)
        # self.cmbFd["value"] = ('禁止', '使能')
        # self.cmbFd.current(0)

        # CAN Data Baudrate
        # tk.Label(self.gbCANCfg, anchor=tk.W, text="数据域波特率:").grid(row=4, column=0, sticky=tk.W)
        # self.cmbDataBaudrate = ttk.Combobox(self.gbCANCfg, width=12, state="readonly")
        # self.cmbDataBaudrate.grid(row=4, column=1, sticky=tk.E)
        # self.cmbDataBaudrate["value"] = ('None', '500Kbps', '1000Kbps')
        # self.cmbDataBaudrate.current(0)

        # resistance enable
        tk.Label(self.gbCANCfg, anchor=tk.W, text="终端电阻:").grid(row=5, column=0, sticky=tk.W)
        self.cmbResEnable = ttk.Combobox(self.gbCANCfg, width=12, state="readonly")
        self.cmbResEnable.grid(row=5, column=1, sticky=tk.E)
        self.cmbResEnable["value"] = ('使能', '禁止')
        self.cmbResEnable.current(0)

        # Open/Close Device
        self.strvDevCtrl = tk.StringVar()
        self.strvDevCtrl.set("打开")
        self.btnDevCtrl = ttk.Button(self.gbCANCfg, textvariable=self.strvDevCtrl, command=self.BtnOpen_Click)
        self.btnDevCtrl.grid(row=6, column=1, columnspan=2, pady=2, sticky=tk.E)

    def DevInfoWidgetsInit(self):
        tk.Label(self.gbDevInfo, anchor=tk.W, text="物理请求地址(hex):").grid(row=0, column=0, sticky=tk.W)
        self.Func_ID = tk.Entry(self.gbDevInfo, width=10)
        self.Func_ID.grid(row=0, column=1, sticky=tk.E)
        self.Func_ID.insert(0, hex(cfg.TX_ID_FUNC))

        tk.Label(self.gbDevInfo, anchor=tk.W, text="功能请求地址(hex):").grid(row=1, column=0, sticky=tk.W)
        self.Phys_ID = tk.Entry(self.gbDevInfo, width=10)
        self.Phys_ID.grid(row=1, column=1, sticky=tk.E)
        self.Phys_ID.insert(0, hex(cfg.TX_ID_PHYS))

        tk.Label(self.gbDevInfo, anchor=tk.W, text="响应地址(hex):").grid(row=2, column=0, sticky=tk.W)
        self.Resp_ID = tk.Entry(self.gbDevInfo, width=10)
        self.Resp_ID.grid(row=2, column=1, sticky=tk.E)
        self.Resp_ID.insert(0, hex(cfg.RX_ID))

        tk.Label(self.gbDevInfo, anchor=tk.W, text="p2_timeout:").grid(row=3, column=0, sticky=tk.W)
        self.P2 = tk.Entry(self.gbDevInfo, width=10)
        self.P2.grid(row=3, column=1, sticky=tk.E)
        self.P2.insert(0, cfg.client_config['p2_timeout'])

        tk.Label(self.gbDevInfo, anchor=tk.W, text="p2_star_timeout:").grid(row=4, column=0, sticky=tk.W)
        self.P2_Star = tk.Entry(self.gbDevInfo, width=10)
        self.P2_Star.grid(row=4, column=1, sticky=tk.E)
        self.P2_Star.insert(0, cfg.client_config['p2_star_timeout'])

        tk.Label(self.gbDevInfo, anchor=tk.W, text="stmin:").grid(row=5, column=0, sticky=tk.W)
        self.Stmin = tk.Entry(self.gbDevInfo, width=10)
        self.Stmin.grid(row=5, column=1, sticky=tk.E)
        self.Stmin.insert(0, cfg.isotp_params['stmin'])

        tk.Label(self.gbDevInfo, anchor=tk.W, text="request_timeout:").grid(row=6, column=0, sticky=tk.W)
        self.Request_timeout = tk.Entry(self.gbDevInfo, width=10)
        self.Request_timeout.grid(row=6, column=1, sticky=tk.E)
        self.Request_timeout.insert(0, cfg.client_config['request_timeout'])

        self.strvDevSet = tk.StringVar()
        self.strvDevSet.set("设置")
        self.btnDevSet = ttk.Button(self.gbDevInfo, textvariable=self.strvDevSet, command=self.BtnDevCtrl_Click)
        self.btnDevSet.grid(row=7, column=1, columnspan=2, pady=2, sticky=tk.E)
        self.strvClear = tk.StringVar()
        self.strvClear.set("清空输出界面")
        self.btnClear = tk.Button(self.gbDevInfo, textvariable=self.strvClear, command=self.clearBox)  # 建一个按键
        self.btnClear.grid(row=7, column=0, columnspan=2, pady=2, sticky=tk.W)

    def DiagWidgetsInit(self):
        # diag part############################################################################################
        self.strvReadDTC = tk.StringVar()
        self.strvReadDTC.set("读取故障码")
        self.btnReadDTC = ttk.Button(self.frameesc, textvariable=self.strvReadDTC, command=self.readDTC)
        self.btnReadDTC.grid(row=0, column=0, padx=3, pady=3)

        self.strvClrDTC = tk.StringVar()
        self.strvClrDTC.set("清除故障码")
        self.btnClrDTC_1 = ttk.Button(self.frameesc, textvariable=self.strvClrDTC, command=self.clearDTC)
        self.btnClrDTC_1.grid(row=0, column=1, padx=3, pady=3)

        self.strvReadAppVer = tk.StringVar()
        self.strvReadAppVer.set("读取APP软件版本")
        self.btnReadSwVer = ttk.Button(self.frameesc, textvariable=self.strvReadAppVer, command=self.readAPPVersion)
        self.btnReadSwVer.grid(row=0, column=2, padx=3, pady=3)

        self.strvReadBtlVer = tk.StringVar()
        self.strvReadBtlVer.set("读取BTL软件版本")
        self.btnReadSwVer = ttk.Button(self.frameesc, textvariable=self.strvReadBtlVer, command=self.readBTLVersion)
        self.btnReadSwVer.grid(row=0, column=3, padx=3, pady=3)

        self.strvINSCali = tk.StringVar()
        self.strvINSCali.set("写入VIN码")
        self.btnINSCali = ttk.Button(self.frameesc, textvariable=self.strvINSCali, command=self.writeVIN)
        self.btnINSCali.grid(row=1, column=0, padx=3, pady=3)

        self.strvResetECU = tk.StringVar()
        self.strvResetECU.set("ECU复位")
        self.btnResetECU = ttk.Button(self.frameesc, textvariable=self.strvResetECU, command=self.ecuRest)
        self.btnResetECU.grid(row=1, column=1, padx=3, pady=3)

        self.strvSafetyAccess = tk.StringVar()
        self.strvSafetyAccess.set("安全解锁")
        self.btnSafetyAccess = ttk.Button(self.frameesc, textvariable=self.strvSafetyAccess, command=self.safetyAccess)
        self.btnSafetyAccess.grid(row=1, column=2, padx=3, pady=3)

    def FlashWidgetsInit(self):
        # flash part############################################################################################
        self.swpath4show = tk.StringVar()
        tk.Label(self.frameeps, text="Application软件路径:").grid(row=1, column=0)
        tk.Entry(self.frameeps, textvariable=self.swpath4show).grid(row=1, column=1)
        self.bootpath4show = tk.StringVar()
        tk.Label(self.frameeps, text="FlashDriver软件路径:").grid(row=0, column=0)
        tk.Entry(self.frameeps, textvariable=self.bootpath4show).grid(row=0, column=1)

        self.btnFilePath = ttk.Button(self.frameeps, text="app路径选择", command=self.BtnSelectSwPath_Click)
        self.btnFilePath.grid(row=1, column=2, padx=5, pady=5)
        self.btnFilePath = ttk.Button(self.frameeps, text="driver路径选择", command=self.BtnSelectBootPath_Click)
        self.btnFilePath.grid(row=0, column=2, padx=5, pady=5)

        self.btnSwFlash = ttk.Button(self.frameeps, text="开始刷写", command=self.download)
        self.btnSwFlash.grid(row=2, column=0, columnspan=2, padx=10, pady=10)
        self.progressbarOne = tkinter.ttk.Progressbar(self.frameeps)
        self.progressbarOne.grid(row=3, column=0, columnspan=2, padx=10, pady=10, ipadx=100)
        self.progressbarOne['maximum'] = 100
        self.labelProgressbar = ttk.Label(self.frameeps, text='')
        self.labelProgressbar.grid(row=3, column=2, columnspan=2)
        self.gettime()

    def TestWidgetsInit(self):
        # test part##############################################################################################
        self._v = IntVar()
        self.Radioesc = Radiobutton(self.frametest, text='0x10', variable=self._v,
                                    value=1)  # TODO. need to add function for switching addressing
        self.Radioesc.grid(row=0, column=0, padx=3, pady=3)
        self.Radioeps = Radiobutton(self.frametest, text='0x27', variable=self._v, value=2)
        self.Radioeps.grid(row=0, column=1, padx=3, pady=3)
        self.Radioepb = Radiobutton(self.frametest, text='0x28', variable=self._v, value=3)
        self.Radioepb.grid(row=0, column=2, padx=3, pady=3)

        self.btnAutoDiagTest = ttk.Button(self.frametest, text="开始测试")
        self.btnAutoDiagTest.grid(row=13, column=0, padx=3, pady=3)
        self.btnAutoDiagTest["state"] = tk.DISABLED
        self.btnExportReport = ttk.Button(self.frametest, text="输出测试报告", )
        self.btnExportReport.grid(row=13, column=1, padx=3, pady=3)
        self.btnExportReport["state"] = tk.DISABLED
        self.btnClrReport = ttk.Button(self.frametest, text="清空屏幕", )
        self.btnClrReport.grid(row=13, column=2, padx=3, pady=3)
        self.btnClrReport["state"] = tk.DISABLED

    def FileWidgetsInit(self):
        # file part############################################################################################
        self.btnFile1show = tk.StringVar()
        tk.Label(self.framefile, text="文件1路径:").grid(row=0, column=0)
        tk.Entry(self.framefile, textvariable=self.btnFile1show).grid(row=0, column=1)
        self.btnFile2show = tk.StringVar()

        self.btnFile1Path = ttk.Button(self.framefile, text="文件1路径选择", command=self.BtnSelectFile1_Click)
        self.btnFile1Path.grid(row=0, column=2, padx=5, pady=5)

        self.btnFile1sizemin = tk.StringVar()
        self.btnFile1sizemax = tk.StringVar()
        tk.Label(self.framefile, text="文件1地址范围:").grid(row=1, column=0, sticky=tk.W)
        tk.Entry(self.framefile, textvariable=self.btnFile1sizemin).grid(row=1, column=1)
        tk.Entry(self.framefile, textvariable=self.btnFile1sizemax).grid(row=2, column=1)

        tk.Label(self.framefile, text="文件2路径:").grid(row=3, column=0)
        tk.Entry(self.framefile, textvariable=self.btnFile2show).grid(row=3, column=1)
        self.btnFile2Path = ttk.Button(self.framefile, text="文件2路径选择", command=self.BtnSelectFile2_Click)
        self.btnFile2Path.grid(row=3, column=2, padx=5, pady=5)

        self.btnFile2sizemin = tk.StringVar()
        self.btnFile2sizemax = tk.StringVar()
        tk.Label(self.framefile, text="文件2地址范围:").grid(row=4, column=0, sticky=tk.W)
        tk.Entry(self.framefile, textvariable=self.btnFile2sizemin).grid(row=4, column=1)
        tk.Entry(self.framefile, textvariable=self.btnFile2sizemax).grid(row=5, column=1)

        tk.Label(self.framefile, text="输出类型:").grid(row=6, column=0)
        self.cmbFilIdx = ttk.Combobox(self.framefile, width=10, state="readonly")
        self.cmbFilIdx.grid(row=6, column=1)
        self.cmbFilIdx["value"] = ('S19', 'HEX', 'BIN')
        self.cmbFilIdx.current(0)

        self.cmbFilZero = ttk.Combobox(self.framefile, width=10, state="readonly")
        self.cmbFilZero.grid(row=6, column=2)
        self.cmbFilZero["value"] = ('00', 'FF')
        self.cmbFilZero.current(0)

        self.btnSwFlash = ttk.Button(self.framefile, text="输出文件", command=self.CoverFile)
        self.btnSwFlash.grid(row=6, column=3, columnspan=2, padx=10, pady=10)

    def AnalysisWidgetsInit(self):
        # analysis part############################################################################################
        self.btnDbcFileshow = tk.StringVar()
        tk.Label(self.analysis, text="DBC文件路径:").grid(row=0, column=0, sticky=tk.W)
        tk.Entry(self.analysis, textvariable=self.btnDbcFileshow).grid(row=0, column=1)
        self.btnDbcFilePath = ttk.Button(self.analysis, text="DBC文件路径选择", command=self.BtnDbcFilePath_Click)
        self.btnDbcFilePath.grid(row=0, column=2, padx=5, pady=5, sticky=tk.W)
        self.CbtnFpmvar = tkinter.BooleanVar()
        self.CbtnFpm = Checkbutton(self.analysis, text="创建encode函数", variable=self.CbtnFpmvar, onvalue=True,
                                   offvalue=False)
        self.CbtnFpm.grid(row=2, column=0, sticky=tk.W)
        self.CbtnBfvar = tkinter.BooleanVar()
        self.CbtnBf = Checkbutton(self.analysis, text="使用位字段构建struct", variable=self.CbtnBfvar, onvalue=True,
                                  offvalue=False)
        self.CbtnBf.grid(row=2, column=1, sticky=tk.W)
        self.CbtnUfvar = tkinter.BooleanVar()
        self.CbtnUf = Checkbutton(self.analysis, text="使用float类型", variable=self.CbtnUfvar, onvalue=True,
                                  offvalue=False)
        self.CbtnUf.grid(row=2, column=2, sticky=tk.W)
        self.btnCFileOut = ttk.Button(self.analysis, text="输出文件", command=self.BtnAnalysis_Click)
        self.btnCFileOut.grid(row=3, column=2, columnspan=2, padx=10, pady=10, sticky=tk.W)

        self.btnDbcFileLoad = ttk.Button(self.analysis, text="加载DBC", command=self.BtnDBC_Load_Click)
        self.btnDbcFileLoad.grid(row=1, column=2, padx=5, pady=5, sticky=tk.W)

        tk.Label(self.analysis, text="输出节点选择:").grid(row=1, column=0, sticky=tk.W)
        self.cmbMsgName = ttk.Combobox(self.analysis, width=16, state="readonly")
        self.cmbMsgName.grid(row=1, column=1, sticky=tk.W)
        self.cmbMsgName["value"] = self.messagename
        # self.cmbMsgName.current(0)

    def BtnDbcFilePath_Click(self):
        self.AnalysisDbcFilePath = filedialog.askopenfilename()
        self.btnDbcFileshow.set(self.AnalysisDbcFilePath)

    def BtnDBC_Load_Click(self):
        if self.AnalysisDbcFilePath is not None:
            self.db = None
            i = 0
            self.messagename = []
            try:
                self.db = cantools.database.load_file(self.AnalysisDbcFilePath)
                messages = [Message(message) for message in self.db.messages]
                for message in messages:
                    if message.senders:
                        if message.senders not in self.messagename:
                            self.messagename.append(message.senders)
                self.cmbMsgName["value"] = self.messagename
                self.logger.info("DBC载入成功")
            except:
                self.logger.error("DBC载入失败")

    def BtnAnalysis_Click(self):
        if self.AnalysisDbcFilePath is not None:
            header, source, fuzzer_source, fuzzer_makefile = my_generate(database=self.db, database_name="Construct",
                                                                         header_name="header.h",
                                                                         source_name="source.c",
                                                                         fuzzer_source_name="fuzzer_source.c",
                                                                         floating_point_numbers=self.CbtnFpmvar.get(),
                                                                         bit_fields=self.CbtnBfvar.get(),
                                                                         use_float=self.CbtnUfvar.get(),
                                                                         node_name=None)
            f = open("./generate_code/header.h", "w+", encoding="cp1252")
            f.write(header)
            f.close()
            f = open("./generate_code/source.c", "w+", encoding="utf-8")
            f.write(source)
            f.close()
            f = open("./generate_code/fuzzer_source.c", "w", encoding="utf-8")
            f.write(fuzzer_source)
            f.close()
            f = open("./generate_code/fuzzer_makefile.mk", "w", encoding="utf-8")
            f.write(fuzzer_makefile)
            f.close()
            self.logger.info("文件生成成功")
        else:
            self.logger.info("请选择DBC")

    def BtnSelectFile1_Click(self):
        self.ConverFile1Path = filedialog.askopenfilename()
        self.btnFile1show.set(self.ConverFile1Path)

    def BtnSelectFile2_Click(self):
        self.ConverFile2Path = filedialog.askopenfilename()
        self.btnFile2show.set(self.ConverFile2Path)

    # add
    def BtnSelectSwPath_Click(self):
        self.APPFilePath = filedialog.askopenfilename()
        self.swpath4show.set(self.APPFilePath)

    def BtnSelectBootPath_Click(self):
        self.FBLFilePath = filedialog.askopenfilename()
        self.bootpath4show.set(self.FBLFilePath)

    def NeoViConnect(self):
        global bus, client, stack
        try:
            bus = NeoViBus(channel=1)
            stack = isotp.CanStack(bus=bus, address=tp_physaddr,
                                   params=isotp_params)  # Network/Transport layer (IsoTP protocol)
            stack.set_sleep_timing(0, 0)
            conn = PythonIsoTpConnection(stack)
            conn.open()
            client = Client(conn, config=client_config)

            self.logger.info("设备连接成功")
        except:
            self.logger.info("无设备连接")
        return bus, client, stack

    def CanoeConnect(self):
        # global bus, client, stack
        # try:
        bus = VectorBus(channel=0, bitrate=500000, app_name='Canoe', fd=False, data_bitrate=500000)
        stack = isotp.CanStack(bus=bus, address=tp_physaddr,
                               params=isotp_params)  # Network/Transport layer (IsoTP protocol)
        stack.set_sleep_timing(0, 0)
        conn = PythonIsoTpConnection(stack)
        conn.open()
        client = Client(conn, config=client_config)
        self.logger.info("设备连接成功")
        # except:
        #     self.logger.info("无设备连接")
        return bus, client, stack

    def ToomossConnect(self):
        global bus, client, stack
        try:
            bus = ToomossBus(channel=1)
            stack = isotp.CanStack(bus=bus, address=tp_physaddr,
                                   params=isotp_params)  # Network/Transport layer (IsoTP protocol
            stack.set_sleep_timing(0, 0)
            conn = PythonIsoTpConnection(stack)
            conn.open()
            client = Client(conn, config=client_config)
            self.logger.info("设备连接成功")
        except:
            self.logger.info("无设备连接")
        return bus, client, stack

    def BtnOpen_Click(self):
        if self.isopen is not True:
            if self.cmbDevType.get() == 'canoe':
                self.bus, self.client, self.stack = self.CanoeConnect()
                self.client.open()
            if self.cmbDevType.get() == 'toomoss':
                self.bus, self.client, self.stack = self.ToomossConnect()
                self.client.open()
            if self.cmbDevType.get() == 'valuecan':
                self.bus, self.client, self.stack = self.NeoViConnect()
                self.client.open()
            self.diag_change_client(self.client, self.stack)
            self.flash_change_client(self.client, self.stack)
            self.tests_change_client(self.client, self.stack)
            self.strvDevCtrl.set("关闭")
            self.isopen = True
        else:
            if self.bus is not None:
                self.bus.shutdown()
            self.client.close()
            self.logger.debug("设备关闭")
            self.strvDevCtrl.set("打开")
            self.isopen = False

    def BtnDevCtrl_Click(self):
        self.flash_change_address(
            isotp.Address(isotp.AddressingMode.Normal_11bits, txid=int(self.Phys_ID.get(), base=16),
                          rxid=int(self.Resp_ID.get(), base=16)),
            isotp.Address(isotp.AddressingMode.Normal_11bits, txid=int(self.Func_ID.get(), base=16),
                          rxid=int(self.Resp_ID.get(), base=16)))
        self.diag_change_address(
            isotp.Address(isotp.AddressingMode.Normal_11bits, txid=int(self.Phys_ID.get(), base=16),
                          rxid=int(self.Resp_ID.get(), base=16)),
            isotp.Address(isotp.AddressingMode.Normal_11bits, txid=int(self.Func_ID.get(), base=16),
                          rxid=int(self.Resp_ID.get(), base=16)))
        self.tests_change_address(
            isotp.Address(isotp.AddressingMode.Normal_11bits, txid=int(self.Phys_ID.get(), base=16),
                          rxid=int(self.Resp_ID.get(), base=16)),
            isotp.Address(isotp.AddressingMode.Normal_11bits, txid=int(self.Func_ID.get(), base=16),
                          rxid=int(self.Resp_ID.get(), base=16)))
        logger.debug('配置成功')


def main():
    demo = Widgets()
    demo.mainloop()


if __name__ == '__main__':
    main()
    ws = autosar.workspace(version="4.2.2")
    package = ws.createPackage('DataTypes')
    baseTypes = package.createSubPackage('BaseTypes')
    BaseTypeUint8 = baseTypes.createSwBaseType('uint8', 8, nativeDeclaration='uint8')
    implTypes = package.createSubPackage('ImplementationTypes', role='DataType')
    implTypes.createSubPackage('CompuMethods', role='CompuMethod')
    implTypes.createSubPackage('DataConstrs', role='DataConstraint')
    implTypes.createImplementationDataType('uint8', BaseTypeUint8.ref, 0, 255)
    ws.saveXML('DataTypes.arxml')
    # Load LDF
    # ldf = ldfparser.parse_ldf(path="ekk4.ldf", encoding='utf-8')
    #
    # for frame in ldf.frames:
    #     pass
    # for sig in ldf.signals:
    #     print(sig.name)
    #     print(sig.init_value)
    #     print(sig.encoding_type.get_converters()[0])
    # ldf_generate(ldf)
    # cnt = 0
    # client, stack = connect()
    # Sockets = Test.Socket('COM4', 9600)
    # while True:
    #     cnt += 1
    #     print(cnt)
    #     Sockets.Open()
    #     time.sleep(10)
    #     print(time.localtime())
    #     time.sleep(10)
    #     time.sleep(10)
    #     Sockets.Close()
    #     time.sleep(10)
